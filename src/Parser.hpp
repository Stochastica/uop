#ifndef __UOP_Parser_hpp
#define __UOP_Parser_hpp

#include "CaptureMap.hpp"

namespace uop
{

/**
 * Split the input string along the first split, and move in to the point
 * of the split
 *
 * i.e. --solver=circle will be cleaved to
 *
 *     in     = "=circle"
 *     return = "--solver"
 */
std::string cleave(char const*& in, char const split = '=');

enum class ParseFailure
{
	OPTION_ERROR, // Parsing error in option
	NO_HANDLER,   // No handler found for particular key
};

/**
 * Class for parsing aggregate (int argc, char* argv[]) command line options
 */
class Parser final
{
public:
	Parser& addOption(Option*);

	/**
	 * Populate the options.
	 *
	 * Warning: Changes argv's first level pointers
	 */
	void parse(int argc, char const* argv[]);

	void parse(int argc, char* argv[])
	{
		parse(argc, const_cast<char const** const>(argv));
	}

	std::unordered_map<int, ParseFailure> const&
	failures() const { return failures_; }

	bool success() const { return !failures_.size(); }
private:
	CaptureMap capture_;

	std::unordered_map<int, ParseFailure> failures_;
};

} // namespace uop

#endif // !__UOP_Parser_hpp
