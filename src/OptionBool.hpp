#ifndef __UOP_OptionBool_hpp
#define __UOP_OptionBool_hpp

#include "OptionFlag.hpp"

namespace uop
{


/**
 * This option matches:
 *
 * --flag
 * --flag=0
 * --flag=1
 *
 * -flag
 * -flag=0
 * -flag=1
 */
class OptionBool final: public OptionFlag<bool>
{
public:
	using OptionFlag::OptionFlag;

	virtual bool
	parse(int* const step, int argc, char const* argv[]) override final;
};

} // namespace uop

#endif // !__UOP_OptionBool_hpp
