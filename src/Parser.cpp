#include "Parser.hpp"

#include "Option.hpp"

namespace uop
{

std::string cleave(char const*& in, char const split)
{
	std::string result;

	while (*in && *in != split)
	{
		result += *in;
		++in;
	}

	return result;
}

Parser&
Parser::addOption(Option* opt)
{
	opt->addCapture(capture_);
	return *this;
}

void
Parser::parse(int argc, char const* argv[])
{
	// Parsing loop
	
	int cursor = 0;
	while (cursor < argc)
	{
		// Cleave the option
		std::string key = cleave(argv[cursor]);


		if (capture_.exists(key))
		{
			Option* const opt = capture_[key];
			int step;
			bool const flag = opt->parse(&step, argc - cursor, argv + cursor);
			cursor += step;

			if (!flag)
				failures_[cursor] = ParseFailure::OPTION_ERROR;
		}
		else
		{
			failures_[cursor] = ParseFailure::NO_HANDLER;
			++cursor;
		}
	}
}

} // namespace uop
