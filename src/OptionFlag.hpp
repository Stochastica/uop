#ifndef __UOP_OptionFlag_hpp
#define __UOP_OptionFlag_hpp

#include "Option.hpp"

namespace uop
{

template <typename T>
class OptionFlag: public Option
{
public:
	/**
	 * @param[in] main The main key for this option. e.g. a main key of test will
	 *                 be matched with --test
	 * @param[in] value The default value
	 */
	explicit OptionFlag(std::string main, T const& value):
		keyMain_(main), default_(value)
	{ reset(); }
	/**
	 * @param[in] main The main key for this option. e.g. a main key of test will
	 *                 be matched with --test
	 * @param[in] abbr The secondary key for this option. e.g. a secondary key of
	 *                 tr will be matched with -tr
	 * @param[in] value The default value
	 */
	explicit OptionFlag(std::string main, std::string abbr, T const& value):
		keyMain_(main), keyAbbr_(abbr), default_(value)
	{ reset(); }

	virtual void addCapture(CaptureMap&) override;

	/**
	 * Reset the hit state and value
	 */
	void reset()
	{
		hit_ = false;
		value_ = default_;
	}

	bool isHit() const { return hit_; }
	T value() const { return value_; }
		
protected:

	bool hit_;
	T default_;
	T value_;

private:
	std::string keyMain_;
	std::string keyAbbr_;
};

} // namespace uop

#endif // !__UOP_OptionFlag_hpp
