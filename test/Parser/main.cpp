#include <common/Suite.hpp>

#include <uop/Parser.hpp>
#include <uop/Option.hpp>

using namespace uop;

class OptionTest final: public uop::Option
{
public:
	OptionTest(Suite* const suite,
			std::string key,
			std::string target)
		: suite_(suite)
		, key_(key)
		, target_(target)
		, hit_(false)
	{}

	virtual void addCapture(CaptureMap& map) override
	{
		map.add(key_, this);
	}

	virtual bool parse(int* const step, int argc, char const* argv[]) override
	{
		bool const flag = target_ == std::string(argv[0]);
		suite_->assert(flag, "Anticipated");
		hit_ = true;

		if (!flag)
		{
			std::cerr << "Argc= " << argc
			          << ", Argv[0]= " << argv[0]
			          << '\n';
			return false;
		}
		*step = 1;
		return true;
	}

	bool hit() const { return hit_; }
private:
	Suite* suite_;
	std::string key_;
	std::string target_;
	bool hit_;
};

void case_1(Suite& suite)
{
	char const* arg = "--tree=fir";
	std::string left = cleave(arg);

	suite.assertEqual(left,             "--tree");
	suite.assertEqual(std::string(arg), "=fir");
}

void case_2(Suite& suite)
{
	Parser parser;

	OptionTest t1(&suite, "--verbose", "");
	OptionTest t2(&suite, "--solver", "=fir");

	parser.addOption(&t1);
	parser.addOption(&t2);

	char const* argv[] = {
		"--verbose",
		"--solver=fir",
	};

	parser.parse(2, argv);

	suite.assert(t1.hit(), "Hit 1");
	suite.assert(t2.hit(), "Hit 2");
	suite.assert(parser.success(), "Success");
}

int main()
{
	Suite suite;

	case_1(suite);
	case_2(suite);

	return suite.accumulate();
}
