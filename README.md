# uop, A unified program option parser

## Usage

Below is a minimal example:
```cpp
#include <uop/Parser.hpp>
#include <uop/OptionBool.hpp>
#include <uop/OptionString.hpp>
using namespace uop;
...
Parser parser;

// Args: main key, abbreviated key, default value
OptionBool ob1("verbose", "v", false);
OptionBool ob2("experimental", "exp", false);

OptionString os1("solver", "s", std::string());
OptionString os2("language", "lang", "la");

parser.addOption(&ob1);
parser.addOption(&ob2);
parser.addOption(&os1);
parser.addOption(&os2);

char const* argv[] = {
	"--verbose",
	"-exp=0",
	"--solver=circular",
};

parser.parse(3, argv); // argc, argv

// ob1.value() == true
// ob1.isHit() == true

// os1.value() == "circular" (changed)
// os2.value() == "la"       (default)
```

## Building and Installation

To build uop,
```bash
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build .
```

The `uopConfig.cmake` file will be located in `bin/`. Simply appending this
directory to `CMAKE_PREFIX_PATH` allows `find_package(uop)` to find it. Since
`uop` is a lightweight static library, it can be flexibly linked into different
executables.

## Development and Testing

Building uop in a debug environment requires slightly different flags.
```bash
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug -DTEST=1
cmake --build .
```

